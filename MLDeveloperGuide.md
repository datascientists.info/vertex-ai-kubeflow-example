# ML Developer Guide

A ML development process looks generally like in the picture below.

![alt text](./docs/ml_dev_process.jpg)

There are generally two parts of the process. The Data Engineering part and the Data Science part.

Data Engineering includes loading and preprocessing data, as well as deploying the selected model, while
Data Science is responsible for the training and model evaluation steps.

## ML Pipelines in Kubeflow and Vertex AI
### Kubeflow
Kubeflow is a framework to easily transform python code (and docker containers) into pipelines
executable on a Kubernetes Cluster.
In our defined flow we will work with Python function components whenever possible, which are described here:
https://www.kubeflow.org/docs/components/pipelines/sdk/python-function-components/

### Vertex AI
Vertex AI is an integrated platform for Machine Learning Ops, that includes everything needed for
developing and deploying machine learning model.
As a serverless service resource handling is effortless and on-demand.
Vertex AI Pipelines are Kubeflow V2 pipelines. To generate these, the v2 compiler of Kubeflow is needed.

```
from kfp.v2 import compiler
```

The official documentation can be found here: https://cloud.google.com/vertex-ai/docs/pipelines

## Structuring of a project
### General guidelines
In general a Kubeflow component should always be an independent part of the complete process, that is
runnable by itself, and has defined input and output parameters. All parameters should have typing hints.
To structure the nodes further a separation into two separate python packages is recommended:
* Data Engineering
* Data Science

This should help to identify different tasks / components more easily. 

### Data Engineering
The package __data_engineering__ should include all tasks concerning data and transformation before training,
e.g.:
* loading data (from a database, file)
* preprocessing data (to transform loaded data into the format needed by training)
* deploying the resulting model (in case real time or batch prediction on demand are needed)

### Data Science
The package __data_science__ should include all tasks concerning training and selecting a model, e.g.:
* training models
* evaluate models


## From Python to Kubeflow
Python functions can easily be converted to Kubeflow components, as this is one special use case for Kubeflow, using 
Python function components. To transform a Python function into a Kubeflow component you need to watch some things:
* In- and output parameters
  * Predefined parameters
  * Custom parameters
* Function specifics
* Kubeflow function decorators
* Pipeline function
  * Dependency management
  * Pipeline decorator
  * Pipeline parameters
  * DSL logic components

### In- and output parameters
Parameters are the way components communicate with each other in a pipeline. For this purpose there are some predefined
parameters part of the SDK, but it is also possible to define custom parameters.

#### Predefined parameters
These are especially useful for Kubeflow specific parameters. They later on get automatically interpreted
as input or output, based on the provided type. Kubeflow provides these input and output types:
```
from kfp.v2.dsl import (
    Input,
    Output,
    Dataset,
    Metrics,
    OuputPath
)
```

Input and Output can be further specified by adding the type, e.g. Dataset or Model. This would look like
this ```Output["Dataset"]```. This helps VertexAI with information of the type of artifact it is storing.

If a _Metrics_ type is defined, this enables you to store any kind of information inside the pipeline into
the VertexAI metrics store, like this
```
metrics.log_metric("metric_name", value)
```

These predefined output parameters do not need to be passed by a return statement. Kubeflow automatically recognizes
them as output from the definition.

#### Custom parameters
If you need in- or output parameters, that are not file based, as those provided by Kubeflow above, you can
also use those.

Input parameters can be any type used in any other python code, e.g.
* str
* int
* float
* Dict
* etc

Output parameters need to be defined in a special way, not only be given as a _return_ statement inside the
function / component. They should be defined as __NamedTuples__. And the name of the NamedTuple should be 
_Outputs_ to be in sync with the predefined outputs. A function with this implemented would look like the 
following code.

```
def example(
    parameter1: Dict
) -> NamedTuple("Outputs", [("output_param", str)]):
    from collections import namedtuple

    output_param = "I am an output!"

    output = namedtuple("Outputs", ["output_param"])
    return output(output_param)
```

### Function specifics
Aside the parameters you need to be aware of the following rules to seamlessly convert a Python function into a Kubeflow
component:
* all imports need to be inside the function
  ```
  def component_func(
    parameters: Dict,
  ):
    import pandas as pd
    import numpy as np
    from loguru import logger
  
    do some stuff
  ```
* all function needed inside only a single component need to be defined inside the component main function
  ```
  def component_func(
    parameters: Dict,
  ):
    import pandas as pd
    import numpy as np
    import itertools
    from loguru import logger

    def useful_func(p1):
        do stuff with p1
        return result
  
    result = useful_func(parameters)
  ```

### Kubeflow function decorators
After writing the Python function for the different steps in the pipeline you can add the decorators to them. This then
transforms your functions into a Kubeflow component.

```
from kfp.v2.dsl import component


@component(
    output_component_file="yaml_tmp/component_func.yaml",
    base_image="python:3.9",
    packages_to_install=[
        "google-cloud-bigquery",
        "pandas",
        "pandas-gbq",
        "numpy",
        "loguru",
    ],
)
def component_func(
    parameters: Dict
):
    do something
```

Each ```@component``` decorator takes some parameters:
* output_component_file: name of the Kubeflow definition file that the compiler generates for this component
* base_image: name of the image, that should be used to run the code of the component. Each component can have a 
  different base_image. You can also provide a custom image published to the GCP Container Registry.
* packages_to_install: list of all packages to install in the image of this component

With this way of defining package dependencies you can have lean images and reduce interpackage dependency conflicts, if
the scope of a component is as small as it can be.


### Pipeline function
To tie all components together they are put into a pipeline function, decorated with the ```@pipeline``` decorator. This
pipeline function will then be compiled into the Kubeflow JSON file, that VertexAI needs to deploy and execute the code.
Inside the pipeline function you can also define the dependencies among different components. 

#### Dependency management
In Kubeflow component dependencies can be managed in two ways:
* Implicitly defined dependencies 

  Implicitly defined dependencies work in a way, that one component needs the output of another
  component as input.
  ```
  def pipeline(
      project: str
  ):
      pre = preprocess()
      train = train_base(
          input_data=pre.outputs["output_path"]
      )
  ```
  In this example __train__ is dependent on __pre__, by requiring one of its outputs.


* Explicitly defined dependencies

  Dependencies can also be defined explicitly using the function __after()__
  ```
  def pipeline(
      project: str
  ):
      step1 = step1()
      step2 = step2().after(step1)
  ```
  In this example step2 is dependent on step1 and so defined to need to run __after__ step1.

#### Pipeline decorator
The pipeline Kubeflow decorator tells the Kubeflow compiler which function binds the components into a pipeline.
```
@dsl.pipeline(
    name="pipeline-name",
    pipeline_root="gs://bucket-name",
)
```
This decorator only needs two parameters, the name of the pipeline (can later be found in VertexAI UI) and the GCS 
bucket where the pipeline runs will store all temporary files the results and model artifacts.

#### Pipeline parameters
If you want to start your Kubeflow pipeline with runtime parameters, you can define those as any other Python function
parameters.
```
def pipeline(
    input1: str = 'test'
):
    s1 = step1(input1)
```

When you start this pipeline you can pass the __input1__ parameter into the pipeline creation
```
job = aip.PipelineJob(
    display_name="pipeline-name",
    template_path="pipeline.json".replace(" ", "_"),
    pipeline_root="gs://bucket-name",
    project=project_id,
    location="us-east1",
    credentials=google.oauth2.credentials.Credentials(creds.token),
    enable_caching=False,
    parameter_values={
        "input1": "test3",
    },
)
```

The pipeline parameters go into the dictionary named ```parameter_values```.

#### DSL logic components
You can add logic like loops and if conditions to you pipeline:
* loops: ```with dsl.ParallelFor(list) as value:```
* if condition: ```with dsl.Condition(cond == "true"):``` With these conditions keep in mind, that you it is,
  as of now, not possible to concatenate conditions like ```cond1 == True and cond2 == True``` for this you need
  two conditions in a row with only one entry.

#### Defining component specific resources
You can define specific resources for each component. This includes memory, cpu, gpu and gpu type.
```
c = component()
c.set_memory_limit("32G")
c.set_cpu_limit("8")
c.set_gpu_limit(1)
c.add_node_selector_constraint(
    "cloud.google.com/gke-accelerator",
    "nvidia-tesla-v100",
)
```


## Testing
As all components should be independently running pieces of the pipeline they can be tested with mock or example data
for their input parameters.

To make the components testable locally you need to overwrite the ```@component``` decorators. This can be achieved by
adding this code before importing the component code into the test:
```
from mock import patch

patch(
    "kfp.v2.dsl.component",
    lambda *x, **y: lambda f: f,
).start()

from components.data_science.training import train
```

With this code you can then use all component parameters like normal python function parameters.

The Kubeflow specific input and output parameters can be mocked by creating a __NamedTuple__ inside your test
and adding the needed attributes to it, e.g. for an Input[Dataset]
```
InputStruct = namedtuple("InputStruct", "path")
s = InputStruct(path="./tests/data/predict_input/")
```


## Vertex AI runner
### General Information
#### Caching
Caching can be used in Vertex AI to reuse components, that ran successfully once already. This is a good feature to
speed up development of pipelines, if you do not have to run the complete pipeline every time. For production runs
set this flag to __False__ if the data for the model changes or the model should be updated. Else Vertex AI will always 
reload the last run and new data will not be considered. New data meaning in this case, additional rows, if the input data
format changes, Vertex AI will reprocess the data.

https://cloud.google.com/vertex-ai/docs/pipelines/configure-caching

### Trouble shooting
Trouble shooting can be done in the VertexAI UI (https://console.cloud.google.com/vertex-ai/pipelines?project=data-234917).
Here you can see an overview over all pipelines that were run already or are running right now, with their status.

By clicking on the run name, you get to a graphical representation of the pipeline (if not too complex) and can see
which nodes ran and what their status is. Clicking on a node give you an overview of the input and output parameters 
used for the run and also a button that shows you the logs for this node.

#### No pipeline graph shown
If there is no pipeline graph shown you can find the information for each node here in VertexAI under __Training__ -> 
__Custom Jobs__ (https://console.cloud.google.com/vertex-ai/training/custom-jobs?project=data-234917). You can find
your jobs by filtering using the following pattern:
__caip_pipelines\__[job_id]__

The job_id is provided on each failed run inside the status message from the ended pipeline job, if the
pipeline failed:
```
"The DAG failed because some tasks failed. The failed tasks are: [for-loop-1].; 
Job (project_id = data-234917, job_id = 5455811881396600832) is failed due to the above error.; 
Failed to handle the job: {project_number = 246437554846, job_id = 5455811881396600832}"
```
#### Out of memory error
If you get an out of memory error like this:
```
Custom job failed with error message: The replica workerpool0-0 ran out-of-memory and exited with a non-zero status of 
137(SIGKILL)
```

You can specify more resources for this component, using the following code:
```
c = component()
c.set_memory_limit("32G")
c.set_cpu_limit("8")
```

## Local runner

### Metrics
To store metrics during local training in VertexAI use the following code:

```
import google.cloud.aiplatform as aip

aip.init(project=PROJECT_ID, location=REGION, experiment=EXPERIMENT_NAME)
aip.log_metrics({"eval_loss": loss, "eval_mae": mae, "eval_mse": mse})
```

## Deployment
The project should be deployed automatically using Google Cloud Build and then triggered using an Airflow job
in the data repository (https://github.com/spin-org/data).
As each model has its own subdirectories, e.g. __scooter-loos/__ and __battery-reliability__, each model should 
have subdirectories called ```deployment/``` and ```src/```. Model and business logic belongs under __src/__ while
the Cloud Build files belong under __deployment/__. The __deployment/__ mainly should contain two files:
* cloudbuild.yaml
* Dockerfile

Additionally to that there should always be a ```pipeline_check.py``` file in ```src/``` to kick off the pipeline
if the deployment is run in stage.

### cloudbuild.yaml
The cloudbuild.yaml file should follow this structure and contain at least four steps:
* installing the requirements needed for tests, if any, and the ```pipeline_check.py``` script
* building the container to kick off the pipeline (definition is the ```Dockerfile```)
* pushing the container to the cloud registry
* executing script ```pipeline_check.py``` to kick off the pipeline, if the deployment is in stage

An example of a build file is here
```
steps:
  # Install dependencies
  - name: python:3.9
    entrypoint: pip
    args: [ 'install', '-r', './scooter-loss/src/requirements.txt', '--user' ]

  # build image
  - name: 'gcr.io/cloud-builders/docker'
    args: [
      'build',
      '-t', 'gcr.io/$PROJECT_ID/github.com/spin-org/vertex-ai-scooter-loss:prod',
      '-f', './scooter-loss/deployment/Dockerfile',
      '.'
    ]
  # push container
  - name: 'gcr.io/cloud-builders/docker'
    args: [ 'push', 'gcr.io/$PROJECT_ID/github.com/spin-org/vertex-ai-scooter-loss:prod' ]
  # if in staging kick off run
  - name: python:3.9
    entrypoint: python
    args: [ 'pipeline_check.py' ]
    dir: './scooter-loss/src/'
    env:
      - BRANCH_NAME=$BRANCH_NAME

timeout: 3600s
```

### Dockerfile
The Dockerfile should have all the requirements needed to kick off a VertexAI pipeline, not all packages
required to run the pipeline, as they will be installed inside the different components during runtime.
So it might make sense to have a separate requirements file just for this container. The most common packages
are:
* google-cloud-aiplatform
* google-auth
* kfp

A Dockerfile could then look like this:
```
FROM python:3.9-buster

RUN apt-get update

WORKDIR /app

COPY scooter-loss/src/ .

RUN pip3 install -r requirements_docker.txt

CMD [ "python", "pipeline_runner_vertex_ai.py" ]
```

### pipeline_check.py
This script checks if the pipeline is already running in the stage project, and if not, kicks it off.

Here an example
```
import google.oauth2.credentials
import google.cloud.aiplatform as aip

from kfp.v2 import compiler

import google.auth
import google.auth.transport.requests

import sys
import os
from loguru import logger

from pipeline import pipeline


scopes = ["https://www.googleapis.com/auth/cloud-platform"]
creds, project = google.auth.default(scopes=scopes)

auth_req = google.auth.transport.requests.Request()
creds.refresh(auth_req)

if os.getenv("BRANCH_NAME") == "stage":
    logger.info("in stage branch")
    jobs = aip.PipelineJob.list(
        filter='displayName="vehicle-loss-training"',
        credentials=google.oauth2.credentials.Credentials(creds.token),
        project="data-dev-247200",
        location="us-east1",
    )

    running_flag = False

    for job in jobs:
        d_job = job.to_dict()
        if (
            d_job["displayName"] == "vehicle-loss-training"
            and d_job["state"] == "PIPELINE_STATE_RUNNING"
        ):
            logger.info("already a job running")
            running_flag = True
            sys.exit(1)

    if running_flag is False:
        compiler.Compiler().compile(
            pipeline_func=pipeline, package_path="pipeline.json".replace(" ", "_")
        )

        copes = ["https://www.googleapis.com/auth/cloud-platform"]
        creds, project = google.auth.default(scopes=scopes)

        auth_req = google.auth.transport.requests.Request()
        creds.refresh(auth_req)

        job = aip.PipelineJob(
            display_name="vehicle-loss-training",
            template_path="pipeline.json".replace(" ", "_"),
            pipeline_root="gs://kedro-test-spin",
            project="data-dev-247200",
            location="us-east1",
            credentials=google.oauth2.credentials.Credentials(creds.token),
            enable_caching=False,
            parameter_values={
                "project": "data-dev-247200",
                "display_name": "vehicle-loss-training",
                "thresholds_dict_str": {"auRoc": 0.3},
                "budget_milli_node_hours": 1,
                "training_data_table": "bq://data-dev-247200.test_ml_outputs.v_scooter_loss_input",
            },
        )

        service_account = "647650724511-compute@developer.gserviceaccount.com"

        job.submit(
            service_account=service_account,
        )

else:
    sys.exit(0)
```

### Cloud Build Trigger
You can define several triggers on the same branch, to only run if files in a defined directory were changed.

This can be done using the __Included files filter__ as shown below.

![alt text](./docs/cloud_build_trigger_with_filemask.png)

## Model Monitoring
Once a model is deployed to an endpoint it can be monitored to be alerted on skew and drift detection:
* For skew detection, the baseline is the statistical distribution of the feature's values in the 
  training data.
* For drift detection, the baseline is the statistical distribution of the feature's values seen in 
  production in the recent past.

Instructions to set this up in the console can be found here:
https://cloud.google.com/vertex-ai/docs/model-monitoring/overview

An example on how to set it up programmatically an be found here:
https://github.com/GoogleCloudPlatform/vertex-ai-samples/blob/main/notebooks/official/model_monitoring/model_monitoring.ipynb


## Best Practices

