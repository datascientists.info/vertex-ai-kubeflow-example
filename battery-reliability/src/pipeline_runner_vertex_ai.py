import json
import os
import sys
from kfp.v2 import compiler

import google.oauth2.credentials
import google.cloud.aiplatform as aip

import google.auth
import google.auth.transport.requests

from argparse import ArgumentParser
from loguru import logger

from pipeline import pipeline


def main(args=None):
    if args is None:
        parser = ArgumentParser()
        parser.add_argument(
            "-p",
            "--project_id",
            nargs="?",
            const="PROJECT_ID",
            dest="project_id",
            help="gcp project_id",
            metavar="PROJECT_ID",
        )
        parser.add_argument(
            "-min_data",
            "--min_data",
            nargs="?",
            const=1000,
            dest="min_data",
            help="min data required for training",
            metavar="MIN_DATA",
        )
        parser.add_argument(
            "-bucket",
            "--bucket",
            nargs="?",
            const="BUCKET_NAME",
            dest="bucket",
            help="bucket used during run",
            metavar="BUCKET",
        )
        parser.add_argument(
            "-permanent_model_path",
            "--permanent_model_path",
            nargs="?",
            const="models/",
            dest="permanent_model_path",
            help="path to permanently store models on bucket",
            metavar="PERMANENT_MODEL_PATH",
        )
        parser.add_argument(
            "-permanent_path_bq_load",
            "--permanent_path_bq_load",
            nargs="?",
            const="model-bq-load/",
            dest="permanent_path_bq_load",
            help="path to permanently store files to load into bigquery (predictions and threshold data)",
            metavar="PERMANENT_PATH_BQ_LOAD",
        )
        parser.add_argument(
            "-training_parameters",
            "--training_parameters",
            nargs="?",
            const=json.dumps(
                {
                    "training": {
                        "FRACTION": 0.5,
                        "N_CORES": 4,
                        "N_CHAINS": 4,
                        "BURNING_FRACTION": 0.2,
                        "SEED": 3,
                        "LB_RELIABILITY": 0.5,
                        "DEFAULT_THRESHOLD": 24,
                    }
                }
            ),
            dest="training_parameters",
            help="""
                JSON of training parameters with the following structure:
                {
                    "training": {
                        "FRACTION": 0.5,
                        "N_CORES": 4,
                        "N_CHAINS": 4,
                        "BURNING_FRACTION": 0.2,
                        "SEED": 3,
                        "LB_RELIABILITY": 0.5,
                        "DEFAULT_THRESHOLD": 24,
                    }
                },
            """,
        )
        parser.add_argument(
            "-battery_reliability_table",
            "--battery_reliability_table",
            nargs="?",
            const="test_ml_outputs.vertex_ai_battery_reliability",
            dest="battery_reliability_table",
            help="table name to load battery_reliability into. in format <dataset>.<tablename>",
        )
        parser.add_argument(
            "-battery_threshold_table",
            "--battery_threshold_table",
            nargs="?",
            const="test_ml_outputs.vertex_ai_battery_reliability",
            dest="battery_threshold_table",
            help="table name to load battery_threshold into. in format <dataset>.<tablename>",
        )

        creds = os.getenv("GOOGLE_CREDENTIALS", None)
        if creds:
            with open("srv_acct.json", "w") as f:
                f.write(creds)
            os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./srv_acct.json"
        else:
            print("in else")
            logger.error("No credentials passed into environment")
            sys.exit(1)

        args = parser.parse_args()

        compiler.Compiler().compile(
            pipeline_func=pipeline, package_path="pipeline.json".replace(" ", "_")
        )

        scopes = ["https://www.googleapis.com/auth/cloud-platform"]
        creds, project = google.auth.default(scopes=scopes)

        auth_req = google.auth.transport.requests.Request()
        creds.refresh(auth_req)

        job = aip.PipelineJob(
            display_name="battery-reliability",
            template_path="pipeline.json".replace(" ", "_"),
            pipeline_root="gs://" + args.bucket,
            project=args.project_id,
            location="us-east1",
            credentials=google.oauth2.credentials.Credentials(creds.token),
            enable_caching=False,
            parameter_values={
                "project": args.project_id,
                "min_data_required": args.min_data,
                "bucket": args.bucket,
                "permanent_path": args.permanent_model_path,
                "permanent_path_bq_load": args.permanent_path_bq_load,
                "parameters": json.loads(args.training_parameters),
                "battery_reliability_table": args.battery_reliability_table,
                "battery_threshold_table": args.battery_threshold_table,
            },
        )

        service_account = "sa@address.com"

        job.run(
            service_account=service_account,
        )


if __name__ == "__main__":
    main(args=None)
