# Battery Reliability Training

## Pipeline Overview
![Pipeline](../../docs/vertext_ai_battery_reliability_flow.png)

## Running training
To run the VertexAI job locally call __pipeline_generator.py__ with the following arguments:
* -p: gcp project_id to run job in (e.g. data-dev-247200)
* -min_data: minimal amount of data for a super region scooter combination to train a model 
* -bucket: gcs bucket where vertexAi stores results in and permanent data is kept 
* -permanent_model_path: path on bucket to store trained models in. Always add "/" (e.g.models/) 
* -permanent_path_bq_load: path in bucket to store files for bigquery upload in. Always add "/" (e.g. model-bq-load/)
* -training_parameters: parameters used for training the model, in JSON format like this: 
    ```
  {
    "training": {
      "FRACTION": 0.5,
      "N_CORES": 4,
      "N_CHAINS": 4,
      "BURNING_FRACTION": 0.2,
      "SEED": 3,
      "LB_RELIABILITY": 0.5,
      "DEFAULT_THRESHOLD": 24,
    }
  }
  ```

And set an environment variable ```GOOGLE_CREDENTIALS``` to contain the contents of a gcloud service account
json file. This can be achieved by running this command:
```
export GOOGLE_CREDENTIALS=$(cat pat_to_service_account.json)
```

Example commandline call:
```
python pipeline_runner_vertex_ai.py -p data-dev-247200 -min_data 1000 -bucket kedro-test-spin -permanent_model_path models/ -permanent_path_bq_load model-bq-load/ --training_parameters "{\"training\": {\"FRACTION\": 0.5, \"N_CORES\": 4, \"N_CHAINS\": 4, \"BURNING_FRACTION\": 0.2, \"SEED\": 3, \"LB_RELIABILTY\": 0.5, \"DEFAULT_THRESHOLD\": 24, \"NUM_SAMPLES\": 2000, \"LB_RELIABILITY\": 0.5}}" --battery_reliability_table test_ml_outputs.vertex_ai_battery_reliability --battery_threshold_table test_ml_outputs.vertex_ai_battery_threshold
```

