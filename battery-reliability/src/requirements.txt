kfp
pandas==1.2.4
pandas-gbq
numpy<1.20
loguru
scipy
joblib
pytest
mock
pymc3==3.9.3
arviz==0.11.0
sklearn

google-cloud-aiplatform
google-cloud-bigquery
google-cloud-storage
gcsfs
