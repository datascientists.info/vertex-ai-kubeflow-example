from typing import Dict, NamedTuple

from kfp.v2.dsl import component
from kfp.v2.dsl import OutputPath


@component(
    output_component_file="yaml_tmp/super_region_ids.yaml",
    base_image="python:3.9",
    packages_to_install=[
        "google-cloud-bigquery",
        "pandas",
        "pandas-gbq",
    ],
)
def get_super_regionid(
    project: str,
) -> NamedTuple("Outputs", [("super_region_ids", Dict)]):
    """
    load relation of super region name to id
    :param project: gcp project to execute query in
    :return: Dict of relation name to id
    """
    import pandas as pd

    query = f"""select distinct(super_region_name), super_region_id from warehouse.dim_regions"""
    super_region_df = pd.read_gbq(query, project_id=project)

    d_super_regions = {
        row["super_region_name"]: row["super_region_id"]
        for _, row in super_region_df.iterrows()
    }

    from collections import namedtuple

    output = namedtuple("Outputs", ["super_region_ids"])
    return output(d_super_regions)


@component(
    output_component_file="yaml_tmp/vehicle_ids.yaml",
    base_image="python:3.9",
    packages_to_install=[
        "google-cloud-bigquery",
        "pandas",
        "pandas-gbq",
    ],
)
def get_vehicleid(project: str) -> NamedTuple("Outputs", [("vehicle_ids", Dict)]):
    """
    load relation of vehicle name to id
    :param project: gcp project to execute query in
    :return: Dict of relation name to id
    """
    import pandas as pd

    query = f"""select distinct(scooter_type), vehicle_version_id from warehouse.dim_vehicle_versions"""
    vehicle_df = pd.read_gbq(query, project_id=project)
    d_vehicles = {
        row["scooter_type"]: row["vehicle_version_id"]
        for _, row in vehicle_df.iterrows()
    }

    from collections import namedtuple

    output = namedtuple("Outputs", ["vehicle_ids"])
    return output(d_vehicles)


@component(
    output_component_file="yaml_tmp/super_regions.yaml",
    base_image="python:3.9",
    packages_to_install=[
        "google-cloud-bigquery",
        "pandas",
        "pandas-gbq",
    ],
)
def get_super_regions(
    project: str,
) -> NamedTuple("Outputs", [("regions", list)]):
    """
    load all super regions for loop in pipeline
    :param project: gcp project_id
    :return: list of all super region name
    """
    import pandas as pd

    query = f"""select distinct super_region_name from warehouse.dim_regions"""
    super_region_df = pd.read_gbq(query, project_id=project)

    from collections import namedtuple

    output = namedtuple("Outputs", ["regions"])
    return output(super_region_df["super_region_name"].values.tolist())


@component(
    output_component_file="yaml_tmp/vehicles.yaml",
    base_image="python:3.9",
    packages_to_install=[
        "google-cloud-bigquery",
        "pandas",
        "pandas-gbq",
    ],
)
def get_vehicles(
    project: str, super_region: str
) -> NamedTuple("Outputs", [("vehicles", list)]):
    """
    load all vehicle types for a super region for loop in pipeline
    :param project: gcp project_id
    :return: list of all vehicle types for a super region
    """
    import pandas as pd

    query = """
        SELECT 
            DISTINCT scooter_type  
        FROM (
            SELECT 
                b.scooter_type,
                r.super_region_name,
            FROM `warehouse.dim_vehicles` a 
                INNER JOIN `warehouse.dim_vehicle_versions`  as b ON a.vehicle_version_id =b.vehicle_version_id
                INNER JOIN `warehouse.dim_regions` r ON a.region_id = r.region_id
            WHERE a.stage IN (1,2,3,7,9)
        )
        WHERE super_region_name="${sql.r_name}"
    """
    vehicle_df = pd.read_gbq(
        query.replace("${sql.r_name}", super_region.replace(r"'", r"\\'")),
        project_id=project,
    )

    from collections import namedtuple

    output = namedtuple("Outputs", ["vehicles"])
    return output(vehicle_df["scooter_type"].values.tolist())


@component(
    output_component_file="yaml_tmp/load.yaml",
    base_image="python:3.9",
    packages_to_install=[
        "google-cloud-bigquery",
        "google-cloud-storage",
        "pandas",
        "pandas-gbq",
        "loguru",
    ],
)
def load_data(
    super_region: str,
    scooter_type: str,
    permanent_path: str,
    bucket_name: str,
    min_data_required: int,
    out_data_path: OutputPath("Dataset"),
    project: str,
) -> NamedTuple("Outputs", [("enough_rows", str), ("model_exists", str)]):
    """
    function to load all vehicles of a certain type for a super region for training the model. This function also
    defines the parameters for training the model.
    :param super_region: super region to get vehicles for
    :param scooter_type: scooter type to get vehicles for
    :param permanent_path: path models are stored in permanently for retraining
    :param bucket_name: name of bucket models are stored in
    :param min_data_required: number of vehicle data needed for training
    :param out_data_path: kfp internal path to extracted data
    :return: path to extracted data, flag if number of rows are sufficient, flag if model
            for this combination exists already
    """
    import pandas as pd
    from loguru import logger
    from google.cloud import storage

    batter_query = """
        with init as (
                SELECT
                    vehicle_id,
                    super_region_name,
                    lag(created_at_utc,1) over(partition by vehicle_id order by created_at_utc) previous_status_time,
                    created_at_utc as current_status_time,
                    lag(concat(possession_status,"_",primary_operations_status),1) over(partition by vehicle_id order by created_at_utc) previous_status,
                    lag(possession_status,1) over(partition by vehicle_id order by created_at_utc) previous_status_gen,
                    concat(possession_status,"_",primary_operations_status) as current_status,
                    lag(batt_percentage,1) over(partition by vehicle_id order by created_at_utc) previous_batt_percentage,batt_percentage as current_batt_percentage,
                    round(TIMESTAMP_DIFF(created_at_utc, lag(created_at_utc,1) over(partition by vehicle_id order by created_at_utc), minute)/60,2) as time_difference_hour
                FROM `warehouse.vehicle_states_plus`
                WHERE date_local >= DATE_SUB(CURRENT_DATE(), INTERVAL 8 DAY)
                    AND batt_percentage IS NOT NULL
                    AND rentable IS NOT NULL AND scooter_type='${sql.scoot_type}'
                    AND DATE(created_at_utc) >= DATE_SUB(CURRENT_DATE(), INTERVAL 7 DAY)
                ORDER BY vehicle_id,created_at_utc
              ),
              temp as (
                SELECT
                    *,
                    (CASE WHEN previous_status="deployed_rented" AND  current_status="deployed_ready" THEN 1 ELSE 0 END) is_trip,
                    (CASE WHEN previous_status="deployed_ready" AND  current_status="deployed_rented" THEN 1 ELSE 0 END) is_wait,
                    (CASE WHEN current_status = 'retrieved_charge' THEN 1 ELSE 0 END) is_retrieved
                FROM init
                WHERE previous_status IS NOT NULL
              ),
              battery_life as (
                SELECT
                    super_region_name,
                    vehicle_id,previous_status_time,
                    current_status_time,
                    previous_status,
                    current_status,
                    previous_batt_percentage,
                    current_batt_percentage,
                    time_difference_hour,
                    is_trip,
                    is_wait,
                    is_retrieved,
                    group_id,
                    SUM(
                        CASE
                            WHEN is_retrieved=1 THEN 0
                            else is_trip
                        end
                    ) OVER(PARTITION BY vehicle_id, group_id ORDER BY current_status_time) CumulativeTrip
                FROM(
                    SELECT
                        super_region_name,
                        vehicle_id,
                        previous_status_time,
                        current_status_time,
                        previous_status,
                        current_status,
                        previous_batt_percentage,
                        current_batt_percentage,
                        time_difference_hour,
                        is_trip,
                        is_wait,
                        is_retrieved,
                        SUM(is_trip) OVER(PARTITION BY vehicle_id ORDER BY current_status_time) run_total,
                        SUM(is_retrieved) OVER(partition BY vehicle_id ORDER BY current_status_time) group_id
                    FROM temp
                )
              )
        
              select
                vehicle_id,
                array_agg(current_status_time) status_time,
                array_agg(current_status) status,
                array_agg(current_batt_percentage) batt_percentage,
                array_agg(time_difference_hour) time_between_rise
              FROM battery_life
              WHERE super_region_name='${sql.r_name}'
              GROUP BY vehicle_id
        """

    logger.info("starting query")
    df = pd.read_gbq(
        batter_query.replace("${sql.r_name}", super_region).replace(
            "${sql.scoot_type}", scooter_type
        ),
        project_id=project,
    )
    logger.info("end query")

    df.to_csv(
        out_data_path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv",
        index=False,
    )

    if df.shape[0] >= min_data_required:
        enough_rows = "true"
    else:
        enough_rows = "false"

    name = (
        permanent_path + super_region.replace(" ", "_") + "_" + scooter_type + ".joblib"
    )

    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    stats = storage.Blob(bucket=bucket, name=name).exists(storage_client)
    if stats:
        model_exists = "true"
    else:
        model_exists = "false"

    from collections import namedtuple

    output = namedtuple("Outputs", ["enough_rows", "model_exists"])
    return output(enough_rows, model_exists)
