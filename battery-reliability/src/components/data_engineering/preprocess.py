from typing import Dict, NamedTuple

from kfp.v2.dsl import component
from kfp.v2.dsl import Input, Dataset, OutputPath


@component(
    output_component_file="yaml_tmp/preprocess.yaml",
    base_image="python:3.9",
    packages_to_install=[
        "google-cloud-bigquery",
        "pandas",
        "pandas-gbq",
        "numpy",
        "loguru",
    ],
)
def preprocess(
    parameters: Dict,
    super_region: str,
    scooter_type: str,
    input_path: Input[Dataset],
    output_path: OutputPath("Dataset"),
) -> NamedTuple("Outputs", [("preprocess_empty", str)]):
    import pandas as pd
    import numpy as np
    import itertools
    from loguru import logger

    def make_cumulative(row):
        a = np.asarray(
            list(
                filter(
                    None,
                    row["time_between_rise"]
                    .replace("[", "")
                    .replace("]", "")
                    .split(" "),
                )
            ),
            dtype=np.float64,
            order="C",
        )
        time = a
        time[0] = 0.0
        return np.cumsum(time)

    def chop_array(array: str) -> list:
        array = np.asarray(
            list(filter(None, array.replace("[", "").replace("]", "").split(" "))),
            dtype=np.float64,
            order="C",
        )
        prev_element = 1e6
        window, index = list(), list()
        main_window, index_window = list(), list()
        for i, element in enumerate(array):
            if element <= prev_element:
                window.append(element)
                index.append(i)
                prev_element = element
            else:
                # capture the proceeding window
                main_window.append(window)
                index_window.append(index)
                prev_element = element
                window, index = list(), list()
                window.append(element)
                index.append(i)
        # to capture the last window
        main_window.append(window)
        index_window.append(index)
        filter_array = [True if item[0] > 80 else False for item in main_window]
        return list(itertools.compress(main_window, filter_array)), list(
            itertools.compress(index_window, filter_array)
        )

    def chop_time(data_frame_row):
        time_row = np.asarray(
            list(
                filter(
                    None,
                    data_frame_row.time_between_rise.replace("[", "")
                    .replace("]", "")
                    .split(" "),
                )
            ),
            dtype=np.float64,
            order="C",
        )
        indices = data_frame_row.monotonic[-1]
        time_window = list()
        for i, element in enumerate(indices):
            temp = time_row[element]
            temp[0] = 0.0
            time_window.append(temp)
        return time_window

    def get_cumulative_time(time_row) -> list:
        return list(map(lambda x: np.cumsum(x).tolist(), time_row))

    # check size of df
    battery_data = pd.read_csv(
        input_path.path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv",
        index_col="vehicle_id",
    )

    # convert the time into cumulative information
    bd = battery_data
    bd["time_life"] = bd.apply(make_cumulative, axis=1)
    # chop whole life battery data into multiple samll arrays
    bd["monotonic"] = bd["batt_percentage"].apply(chop_array)
    bd["batt"] = bd["monotonic"].apply(lambda x: x[0])
    bd["time_window"] = bd.apply(chop_time, axis=1)
    bd["cumulative_time_window"] = bd["time_window"].apply(get_cumulative_time)
    time_array = list(
        itertools.chain.from_iterable(
            (itertools.chain.from_iterable(bd.cumulative_time_window))
        )
    )
    batt_array = list(
        itertools.chain.from_iterable((itertools.chain.from_iterable(bd.batt)))
    )

    # final data frame to be fed into Pymc3, here we are unesting all the time and battery information
    time_batt = pd.DataFrame({"time": time_array, "battery": batt_array})
    # adding 1e-5 to make sure its logarithm is not NAN
    time_batt["battery"] = time_batt["battery"].apply(lambda x: x + 1e-5)
    if parameters["training"]["FRACTION"] is not None:
        time_batt.update(time_batt.sample(frac=parameters["training"]["FRACTION"]))

    # return time_batt
    time_batt.to_csv(
        output_path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv",
        index=False,
    )

    if len(time_batt.index) == 0:
        empty = "true"
    else:
        empty = "false"

    from collections import namedtuple

    output = namedtuple("Outputs", ["preprocess_empty"])
    return output(empty)
