from kfp.v2.dsl import component
from kfp.v2.dsl import OutputPath


@component(
    base_image="python:3.9",
    output_component_file="yaml_tmp/batch_data.yaml",
    packages_to_install=["google-cloud-bigquery", "pandas", "pandas-gbq", "loguru"],
)
def get_batch_data(
    super_region: str,
    scooter_type: str,
    batch_data_path: OutputPath("Dataset"),
    project: str = "data-dev-247200",
):
    """
    function to load data for prediction
    :param super_region: super_region to load data for
    :param scooter_type: scooter type to load data for
    :param project:
    :param batch_data_path: internal kfp path to output file for loading in next step
    :return:
    """
    import pandas as pd
    from loguru import logger

    query = """
    SELECT
        *
    FROM (
        SELECT
            a.vehicle_id,
            a.last_gps_location_lat,
            a.last_gps_location_lon,
            a.batt_percentage,
            a.created_at,
            b.scooter_type,
            r.super_region_name,
            CASE
                WHEN stage = 1 THEN 'deployed_available'
                WHEN stage = 2 THEN 'picked_up'
                WHEN stage = 3 THEN 'warehoused'
                WHEN stage = 7 THEN 'rented'
                WHEN stage = 9 THEN 'deployed_unrentable'
            END as scooter_stage
        FROM `warehouse.dim_vehicles` a
            INNER JOIN `warehouse.dim_vehicle_versions`  as b ON a.vehicle_version_id=b.vehicle_version_id
            INNER JOIN warehouse.dim_regions r ON a.region_id = r.region_id
        WHERE a.stage in (1,2,3,7,9)
    ) a
    WHERE 1 = 1
        AND super_region_name="${sql.r_name}"
        AND scooter_type="${sql.scoot_type}"
    """

    # read data from bigquery
    df = pd.read_gbq(
        query.replace("${sql.r_name}", super_region).replace(
            "${sql.scoot_type}", scooter_type
        ),
        project_id=project,
    )
    logger.info("end query")

    # save data to kfp shared storage to use in next node
    df.to_csv(
        batch_data_path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv",
        index=False,
    )
