from kfp.v2.dsl import component
from kfp.v2.dsl import Input, Dataset, OutputPath


@component(
    packages_to_install=["pandas", "pandas-gbq", "google-cloud-bigquery", "loguru"],
    base_image="python:3.9",
    output_component_file="yaml_tmp/load_to_bq.yaml",
)
def load_result_to_bq(
    data: Input[Dataset],
    table_name: str,
    super_region: str,
    scooter_type: str,
    out_data_path: OutputPath("Dataset"),
    project: str = "PROJECT_ID",
):
    """
    function to load data from local storage to bigquery.
    :param data: kfp internal path to file to load.
    :param table_name: name of table to load data to
    :param super_region: super region to load data for
    :param scooter_type: scooter type to load data for
    :param out_data_path: kfp internal path where json file to load data is stored.
    :param project: gcs project_id
    :return:
    """
    import pandas as pd
    import time
    from google.cloud import bigquery
    from loguru import logger

    # load file back into dataframe. dtypes have to be defined, else load will fail.
    df = pd.read_csv(
        data.path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv",
        dtype={
            "vehicle_id": int,
            "reliability": float,
            "last_gps_location_lat": float,
            "last_gps_location_lon": float,
            "batt_percentage": int,
            "created_at": str,
            "scooter_type": str,
            "super_region_name": str,
            "scooter_stage": str,
            "meta_insert_ts": int,
        },
    )

    # adding timestamp to data, to later identify most recent entries.
    df["meta_insert_ts"] = int(time.time())

    # data is loaded into bigquery using json newline delimited file
    df.to_json(
        out_data_path + super_region.replace(" ", "_") + "_" + scooter_type + ".json",
        orient="records",
        lines=True,
    )

    client = bigquery.Client(project=project)
    dataset_ref = client.dataset(table_name.split(".")[0])
    dataset = bigquery.Dataset(dataset_ref)
    table_ref = dataset.table(table_name.split(".")[1])
    table = client.get_table(table_ref)

    job_config = bigquery.LoadJobConfig()
    job_config.source_format = bigquery.SourceFormat.NEWLINE_DELIMITED_JSON
    job_config.autodetect = True
    job_config.write_disposition = "WRITE_APPEND"
    job_config.schema = table.schema

    with open(
        out_data_path + super_region.replace(" ", "_") + "_" + scooter_type + ".json",
        "rb",
    ) as f:
        job = client.load_table_from_file(f, table_name, job_config=job_config)

    logger.info("Loaded {} rows into {}.".format(job.output_rows, table_name))


@component(
    packages_to_install=["google-cloud-bigquery"],
    base_image="python:3.9",
    output_component_file="yaml_tmp/load_to_bq.yaml",
)
def load_from_gcs_to_bq(
    table_name: str,
    bucket_name: str,
    permanent_path: str,
    prefix: str,
    project: str = "data-dev-247200",
):
    """
    function to load data from gcs csv to bigquery. files with the same prefix are loaded in one go to prevent
    quota issues resulting from parallel load of single files.
    :param table_name: name of table to load to
    :param bucket_name: name of bucket where files are stored
    :param permanent_path: path in bucket where files are stored
    :param prefix: prefix all files share that should be loaded in one go
    :param project: gcs project_id
    :return:
    """
    from google.cloud import bigquery

    client = bigquery.Client(project=project)
    dataset_ref = client.dataset(table_name.split(".")[0])
    dataset = bigquery.Dataset(dataset_ref)
    table_ref = dataset.table(table_name.split(".")[1])
    table = client.get_table(table_ref)

    job_config = bigquery.LoadJobConfig()
    job_config.source_format = bigquery.SourceFormat.CSV
    job_config.autodetect = True
    job_config.write_disposition = "WRITE_APPEND"
    job_config.schema = table.schema

    uri = "gs://" + bucket_name + "/" + permanent_path + prefix + "*"

    load_job = client.load_table_from_uri(uri, table_name, job_config=job_config)

    load_job.result()
