from kfp.v2.dsl import component
from kfp.v2.dsl import (
    Input,
    Output,
    Artifact,
    Model
)


@component(
    packages_to_install=["google-cloud-aiplatform"],
    base_image="python:3.8",
    output_component_file="yaml_tmp/deploy_component.yaml",
)
def deploy_model(
        model: Input[Model],
        project: str,
        region: str,
        vertex_endpoint: Output[Artifact],
        vertex_model: Output[Model]
):
    from google.cloud import aiplatform

    aiplatform.init(project=project, location=region)

    deployed_model = aiplatform.Model.upload(
        display_name="model-pipeline",
        artifact_uri=model.uri.replace("out_model", ""),
        serving_container_image_uri="us.gcr.io/PROJECT/predictor:latest",
        serving_container_predict_route="/predict",
        serving_container_health_route="/healthcheck"
    )
    endpoint = deployed_model.deploy(machine_type="n1-standard-4")

    # Save data to the output params
    vertex_endpoint.uri = endpoint.resource_name
    vertex_model.uri = deployed_model.resource_name