from kfp.v2.dsl import component
from kfp.v2.dsl import Input, Output, Model

from typing import NamedTuple


@component(
    output_component_file="yaml_tmp/get_reliability.yaml",
    base_image="python:3.8",
    packages_to_install=[
        "google-cloud-bigquery",
        "pandas==1.2.4",
        "numpy<1.20",
        "pymc3==3.9.3",
        "arviz==0.11.0",
        "joblib",
        "loguru",
        "scipy",
    ],
)
def get_reliability(
    in_model: Input[Model],
    out_model: Output[Model],
    super_region: str,
    scooter_type: str,
    battery_obs=None,
    t: int = 24,
) -> NamedTuple("Outputs", [("reliability_elements", str)]):
    """
    function to calculate and store reliability.
    :param in_model: model to be used for input
    :param out_model: kfp internal path for output of model
    :param super_region: super region name to calculate for
    :param scooter_type: scooter type to calculate for
    :param battery_obs:
    :param t:
    :return:
    """
    import numpy as np
    import pymc3 as pm
    from scipy.stats import norm
    from joblib import dump, load
    from loguru import logger

    def load_reliability(gcs_path):
        model_name = (
            f"/gcs/{gcs_path}_"
            + super_region.replace(" ", "_")
            + "_"
            + scooter_type
            + ".joblib"
        )
        with open(model_name, "rb") as buffer:
            loaded_data = load(buffer)
        return loaded_data

    def getTime(battery, eap):
        pred_time = (eap.get("intercept", 0.0) - np.log(battery)) / eap.get(
            "slope", 0.0
        )
        pred_time[np.where(pred_time < 0.0)] = 0.0
        return pred_time

    def predict(x_test, in_model):
        """predicts the output from trace"""
        x_test = np.insert(x_test, 0, 0.0, axis=0)

        model = in_model["model"]

        with model:
            pm.set_data({"time": x_test})
            predicted_values = pm.fast_sample_posterior_predictive(
                in_model["trace"], samples=500, random_seed=2
            )["estimation_like"]

        # only take the second column because first one is a dummy value
        output = np.exp(predicted_values[:, 1])
        return np.clip(output, 0.0, 100.0)

    def getProb(samples, threshold=10.0):
        loc = samples.mean()
        scale = samples.std()
        return norm.cdf(threshold, loc, scale)

    model = load_reliability(
        in_model.uri.replace(
            "out_model_" + super_region.replace(" ", "_") + "_" + scooter_type, ""
        )[5:]
    )

    if battery_obs is None:
        battery_obs = np.array(range(100, 0, -1))

    if np.any(battery_obs > 100.0):
        raise ValueError("Battery value greater than 100%")
    if np.any(battery_obs < 0.0):
        raise ValueError("Battery value less than 0%")
        # predict the time-obs at next hour
    time_obs = getTime(battery_obs, model["eap"]) + t
    # predict battery observation atr next "t" hour
    reliability = list()
    for each in time_obs:
        each = np.array([each])
        try:
            y_pred = predict(each, model)
            # predict probability battery is going to die at next hour
            failure_rate = getProb(y_pred, 10.0)
            reliability.append(1 - failure_rate)
        except Exception as e:
            logger.info(str(e))

    dump(
        {"reliability": reliability},
        out_model.path
        + super_region.replace(" ", "_")
        + "_"
        + scooter_type
        + ".joblib",
    )

    from collections import namedtuple

    if len(reliability) > 0:
        rel = "true"
    else:
        rel = "false"
    output = namedtuple("Outputs", ["reliability_elements"])
    return output(rel)
