from kfp.v2.dsl import component
from kfp.v2.dsl import Input, Dataset, Model, OutputPath


@component(
    base_image="python:3.8",
    output_component_file="yaml_tmp/predict.yaml",
    packages_to_install=[
        "joblib",
        "gcsfs",
        "pandas==1.2.4",
        "numpy<1.20",
        "pymc3==3.9.3",
        "arviz==0.11.0",
        "scipy",
        "loguru",
        "google-cloud-storage",
    ],
)
def predict(
    model: Input[Model],
    data: Input[Dataset],
    super_region: str,
    scooter_type: str,
    bucket_name: str,
    permanent_path: str,
    output_data_path: OutputPath("Dataset"),
):
    """
    function to predict battery reliability for each scooter
    :param model: model to use for prediction
    :param data: data to predict for
    :param super_region: super region name for prediction
    :param scooter_type: scooter type for prediction
    :param bucket_name: name of bucket to store predicted data in
    :param permanent_path: path to store predicted data in
    :param output_data_path: kfp internal path where predicted data is stored
    :return:
    """
    import time
    import joblib
    import numpy as np
    import pandas as pd
    from scipy.interpolate import interp1d
    from loguru import logger
    from google.cloud import storage

    def load_reliability(gcs_path):
        model_name = (
            f"/gcs/{gcs_path}"
            + super_region.replace(" ", "_")
            + "_"
            + scooter_type
            + ".joblib"
        )
        with open(model_name, "rb") as buffer:
            loaded_data = joblib.load(buffer)
        return loaded_data

    def get_reliability(battery, use_default, loaded_data):
        """
        :param battery: float
            battery percentage
        :return:
        """
        y = np.array(loaded_data["reliability"])
        x = np.array(range(100, 0, -1))
        interpolate_func = interp1d(x, y, kind="linear")

        # use the default model only when the reliability file does not exist
        if use_default:
            if battery < 10:
                return 0.0
            elif battery < 24.0:
                return 0.25
            else:
                return 0.75
        else:
            if battery < 1.0:
                return 0.0
            elif battery > 100.0:
                return 1.0
            else:
                return interpolate_func(battery)

    loaded_data = {}

    try:
        loaded_data = load_reliability(
            model.uri.replace(super_region.replace(" ", "_") + "_" + scooter_type, "")[
                5:
            ]
        )

        use_default = False
    except FileNotFoundError:
        use_default = True
    except Exception as e:
        use_default = True

    logger.info("here is info")
    logger.info(loaded_data)

    df = pd.read_csv(
        data.path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv",
    )

    filtered_frame = df[df["batt_percentage"].notna()]
    reliability_measurements = list()
    for index, row in filtered_frame.iterrows():
        batt_percentage = row["batt_percentage"]
        reliability_value = get_reliability(batt_percentage, use_default, loaded_data)
        reliability_measurements.append(reliability_value)
    filtered_frame.insert(
        loc=1, column="reliability", value=np.array(reliability_measurements)
    )

    filtered_frame["batt_percentage"] = filtered_frame["batt_percentage"].astype(int)
    filtered_frame["meta_insert_ts"] = int(time.time())

    filtered_frame.to_csv(
        output_data_path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv",
        index=False,
    )

    # upload model to fixed location
    client = storage.Client()

    bucket = client.get_bucket(bucket_name)
    object_name_in_gcs_bucket = bucket.blob(
        permanent_path
        + "predict_"
        + super_region.replace(" ", "_")
        + "_"
        + scooter_type
        + ".csv"
    )
    object_name_in_gcs_bucket.upload_from_filename(
        output_data_path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv",
    )
