from typing import Dict, NamedTuple

from kfp.v2.dsl import component
from kfp.v2.dsl import Input, Output, Dataset, Model, Metrics


@component(
    output_component_file="yaml_tmp/train_base.yaml",
    base_image="python:3.8",
    packages_to_install=[
        "google-cloud-bigquery",
        "google-cloud-storage",
        "pandas==1.2.4",
        "numpy<1.20",
        "pymc3==3.9.3",
        "arviz==0.11.0",
        "joblib",
        "loguru",
    ],
)
def train_base(
    input_data: Input[Dataset],
    super_region: str,
    scooter_type: str,
    bucket_name: str,
    permanent_path: str,
    parameters: Dict,
    metrics: Output[Metrics],
    out_model: Output[Model],
):
    """
    train model, if no model exists for super region x scooter type combination
    :param input_data: data to train model on
    :param super_region: super region name for prediction
    :param scooter_type: scooter type for prediction
    :param parameters: model parameters
    :param bucket_name: name of bucket to store predicted data in
    :param permanent_path: path to store predicted data in
    :param metrics: kfp internal object to store metrics in VertexAI artifact store
    :param out_model: kfp internal path to stored model
    :return:
    """
    import pymc3 as pm
    from joblib import dump
    import pandas as pd
    from google.cloud import storage
    from loguru import logger

    def get_eap(trace: Dict) -> Dict:
        """ "
        Estimates Expected-A-Posteriori
        """
        # hdp_trace = pm.stats.hpd(self.trace)
        return {
            "intercept": trace["intercept"].mean().item(0),
            "slope": trace["slope"].mean().item(0),
        }

    preprocessed_battery_data = pd.read_csv(
        input_data.path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv"
    )

    with pm.Model() as model:
        x = pm.Data("time", preprocessed_battery_data.time.values)
        y = pm.Data("Battery", preprocessed_battery_data.battery.values)
        # data transformation
        log_y = pm.Deterministic("log_y", pm.math.log(y))
        # hyperparameters
        mu_a = pm.Normal("mu_intercept", mu=0.0, sigma=50)
        sigma_a = pm.HalfCauchy("sigma_intercept", 5.0)
        mu_b = pm.Normal("mu_slope", mu=0.0, sigma=50)
        sigma_b = pm.HalfCauchy("sigma_slope", 5.0)
        # Model Parameters
        log_a = pm.Normal("intercept", mu=mu_a, sigma=sigma_a)
        b = pm.Normal("slope", mu=mu_b, sigma=sigma_b)
        # Model error
        eps = pm.HalfCauchy("eps", 5.0)
        estimation = pm.Deterministic("estimation", log_a - b * x)
        estimation_like = pm.Normal(
            "estimation_like", mu=estimation, sigma=eps, observed=log_y
        )
        try:
            # start = pm.find_MAP()
            step = pm.NUTS(target_accept=0.95)
            # pm.NUTS(scaling=start, target_accept=0.95)
            trace = pm.sample(
                parameters["training"]["NUM_SAMPLES"],
                tune=int(
                    parameters["training"]["BURNING_FRACTION"]
                    * parameters["training"]["NUM_SAMPLES"]
                ),
                step=step,
                chains=parameters["training"]["N_CHAINS"],
                cores=parameters["training"]["N_CORES"],
                random_seed=parameters["training"]["SEED"],
            )
        except Exception as e:
            logger.info("in exception training " + str(e))
            step = pm.Metropolis()
            trace = pm.sample(
                parameters["training"]["NUM_SAMPLES"],
                tune=int(
                    parameters["training"]["BURNING_FRACTION"]
                    * parameters["training"]["NUM_SAMPLES"]
                ),
                step=step,
                chains=parameters["training"]["N_CHAINS"],
                cores=parameters["training"]["N_CORES"],
                random_seed=parameters["training"]["SEED"],
            )
        finally:
            eap = get_eap(trace)

        metrics.log_metric("parameters", parameters["training"])
        dump(
            {"model": model, "trace": trace, "eap": eap},
            out_model.path
            + "_"
            + super_region.replace(" ", "_")
            + "_"
            + scooter_type
            + ".joblib",
        )

        # upload model to fixed location
        client = storage.Client()

        bucket = client.get_bucket(bucket_name)
        object_name_in_gcs_bucket = bucket.blob(
            permanent_path
            + super_region.replace(" ", "_")
            + "_"
            + scooter_type
            + ".joblib"
        )
        object_name_in_gcs_bucket.upload_from_filename(
            out_model.path
            + "_"
            + super_region.replace(" ", "_")
            + "_"
            + scooter_type
            + ".joblib",
        )


@component(
    output_component_file="yaml_tmp/train_base2.yaml",
    base_image="python:3.8",
    packages_to_install=[
        "google-cloud-bigquery",
        "google-cloud-storage",
        "pandas==1.2.4",
        "numpy<1.20",
        "pymc3==3.9.3",
        "arviz==0.11.0",
        "joblib",
        "loguru",
    ],
)
def train_base2(
    input_data: Input[Dataset],
    super_region: str,
    scooter_type: str,
    bucket_name: str,
    permanent_path: str,
    parameters: Dict,
    metrics: Output[Metrics],
    out_model: Output[Model],
):
    """
    train model, if no model exists for super region x scooter type combination
    :param input_data: data to train model on
    :param super_region: super region name for prediction
    :param scooter_type: scooter type for prediction
    :param parameters: model parameters
    :param bucket_name: name of bucket to store predicted data in
    :param permanent_path: path to store predicted data in
    :param metrics: kfp internal object to store metrics in VertexAI artifact store
    :param out_model: kfp internal path to stored model
    :return:
    """
    import pymc3 as pm
    from joblib import dump
    import pandas as pd
    from google.cloud import storage
    from loguru import logger

    def get_eap(trace: Dict) -> Dict:
        """ "
        Estimates Expected-A-Posteriori
        """
        # hdp_trace = pm.stats.hpd(self.trace)
        return {
            "intercept": trace["intercept"].mean().item(0),
            "slope": trace["slope"].mean().item(0),
        }

    preprocessed_battery_data = pd.read_csv(
        input_data.path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv"
    )

    with pm.Model() as model:
        x = pm.Data("time", preprocessed_battery_data.time.values)
        y = pm.Data("Battery", preprocessed_battery_data.battery.values)
        # data transformation
        log_y = pm.Deterministic("log_y", pm.math.log(y))
        # hyperparameters
        mu_a = pm.Normal("mu_intercept", mu=0.0, sigma=50)
        sigma_a = pm.HalfCauchy("sigma_intercept", 5.0)
        mu_b = pm.Normal("mu_slope", mu=0.0, sigma=50)
        sigma_b = pm.HalfCauchy("sigma_slope", 5.0)
        # Model Parameters
        log_a = pm.Normal("intercept", mu=mu_a, sigma=sigma_a)
        b = pm.Normal("slope", mu=mu_b, sigma=sigma_b)
        # Model error
        eps = pm.HalfCauchy("eps", 5.0)
        estimation = pm.Deterministic("estimation", log_a - b * x)
        estimation_like = pm.Normal(
            "estimation_like", mu=estimation, sigma=eps, observed=log_y
        )
        try:
            # start = pm.find_MAP()
            step = pm.NUTS(target_accept=0.95)
            # pm.NUTS(scaling=start, target_accept=0.95)
            trace = pm.sample(
                parameters["training"]["NUM_SAMPLES"],
                tune=int(
                    parameters["training"]["BURNING_FRACTION"]
                    * parameters["training"]["NUM_SAMPLES"]
                ),
                step=step,
                chains=parameters["training"]["N_CHAINS"],
                cores=parameters["training"]["N_CORES"],
                random_seed=parameters["training"]["SEED"],
            )
        except Exception as e:
            logger.info("in exception training " + str(e))
            step = pm.Metropolis()
            trace = pm.sample(
                parameters["training"]["NUM_SAMPLES"],
                tune=int(
                    parameters["training"]["BURNING_FRACTION"]
                    * parameters["training"]["NUM_SAMPLES"]
                ),
                step=step,
                chains=parameters["training"]["N_CHAINS"],
                cores=parameters["training"]["N_CORES"],
                random_seed=parameters["training"]["SEED"],
            )
        finally:
            eap = get_eap(trace)

        metrics.log_metric("parameters", parameters["training"])
        dump(
            {"model": model, "trace": trace, "eap": eap},
            out_model.path
            + "_"
            + super_region.replace(" ", "_")
            + "_"
            + scooter_type
            + ".joblib",
        )

        # upload model to fixed location
        client = storage.Client()

        bucket = client.get_bucket(bucket_name)
        object_name_in_gcs_bucket = bucket.blob(
            permanent_path
            + super_region.replace(" ", "_")
            + "_"
            + scooter_type
            + ".joblib"
        )
        object_name_in_gcs_bucket.upload_from_filename(
            out_model.path
            + "_"
            + super_region.replace(" ", "_")
            + "_"
            + scooter_type
            + ".joblib",
        )


@component(
    output_component_file="yaml_tmp/retrain.yaml",
    base_image="python:3.8",
    packages_to_install=[
        "google-cloud-bigquery",
        "google-cloud-storage",
        "pandas==1.2.4",
        "numpy<1.20",
        "pymc3==3.9.3",
        "arviz==0.11.0",
        "joblib",
        "loguru",
    ],
)
def retrain(
    input_data: Input[Dataset],
    super_region: str,
    scooter_type: str,
    bucket_name: str,
    permanent_path: str,
    parameters: Dict,
    metrics: Output[Metrics],
    out_model: Output[Model],
) -> NamedTuple("Outputs", [("retrain_possible", str)]):
    """
    retrain existing models for super region x scooter type combination
    :param input_data: data to retrain model on
    :param super_region: super region name for prediction
    :param scooter_type: scooter type for prediction
    :param parameters: model parameters
    :param bucket_name: name of bucket to store predicted data in
    :param permanent_path: path to store predicted data in
    :param metrics: kfp internal object to store metrics in VertexAI artifact store
    :param out_model: kfp internal path to retrained model
    :return:
    """
    import joblib
    from google.cloud import storage

    import pymc3 as pm
    from pymc3.distributions import Interpolated
    import numpy as np
    import pandas as pd
    from scipy import stats
    from loguru import logger

    def from_posterior(param, samples):
        """
        :param param: str
            Name of the parameter from trace
        :param samples: Array
            trace of the parameter "param" loaded from the pickel file
        :return: pymc3.distribution datatype
            https://docs.pymc.io/api/distributions/continuous.html#pymc3.distributions.continuous.Interpolated
            Univariate probability distribution defined as a linear interpolation of probability density function
            evaluated on some lattice of points
        """
        smin, smax = np.min(samples), np.max(samples)
        width = smax - smin
        _x = np.linspace(smin, smax, 100)
        _y = stats.gaussian_kde(samples)(_x)

        # what was never sampled should have a small probability but not 0,
        # so we'll extend the domain and use linear approximation of density on it
        _x = np.concatenate([[_x[0] - 3 * width], _x, [_x[-1] + 3 * width]])
        _y = np.concatenate([[0], _y, [0]])
        return Interpolated(param, _x, _y)

    def get_eap(in_trace: Dict) -> Dict:
        """
        Estimates Expected-A-Posteriori
        """
        # hdp_trace = pm.stats.hpd(self.trace)
        return {
            "intercept": in_trace["intercept"].mean().item(0),
            "slope": in_trace["slope"].mean().item(0),
        }

    name = (
        permanent_path + super_region.replace(" ", "_") + "_" + scooter_type + ".joblib"
    )

    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    storage.Blob(bucket=bucket, name=name).download_to_filename("/tmp/model.joblib")

    with open("/tmp/model.joblib", "rb") as buff:
        loaded_data = joblib.load(buff)

    trace = loaded_data["trace"]

    preprocessed_battery_data = pd.read_csv(
        input_data.path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv"
    )

    try:
        flag = "true"
        model = pm.Model()
        with model:
            x = pm.Data("time", preprocessed_battery_data.time.values)
            y = pm.Data("Battery", preprocessed_battery_data.battery.values)
            # data transformation
            log_y = pm.Deterministic("log_y", pm.math.log(y))
            log_a = from_posterior("intercept", trace["intercept"])
            b = from_posterior("slope", trace["slope"])
            # Model error
            eps = from_posterior("eps", trace["eps"])
            estimation = pm.Deterministic("estimation", log_a - b * x)
            estimation_like = pm.Normal(
                "estimation_like", mu=estimation, sigma=eps, observed=log_y
            )
            step = pm.NUTS(target_accept=0.95)
            trace = pm.sample(
                parameters["training"]["NUM_SAMPLES"],
                tune=int(
                    parameters["training"]["BURNING_FRACTION"]
                    * parameters["training"]["NUM_SAMPLES"]
                ),
                step=step,
                chains=parameters["training"]["N_CHAINS"],
                cores=parameters["training"]["N_CORES"],
                random_seed=parameters["training"]["SEED"],
            )
        eap = get_eap(trace)

        metrics.log_metric("parameters", parameters["training"])

        joblib.dump(
            {"model": model, "trace": trace, "eap": eap},
            out_model.path
            + "_"
            + super_region.replace(" ", "_")
            + "_"
            + scooter_type
            + ".joblib",
        )

        # upload model to fixed location
        client = storage.Client()

        bucket = client.get_bucket(bucket_name)
        object_name_in_gcs_bucket = bucket.blob(
            permanent_path
            + super_region.replace(" ", "_")
            + "_"
            + scooter_type
            + ".joblib"
        )
        object_name_in_gcs_bucket.upload_from_filename(
            out_model.path
            + "_"
            + super_region.replace(" ", "_")
            + "_"
            + scooter_type
            + ".joblib",
        )
    except Exception as e:
        logger.info(str(e))
        flag = "false"

    from collections import namedtuple

    output = namedtuple("Outputs", ["retrain_possible"])
    return output(flag)
