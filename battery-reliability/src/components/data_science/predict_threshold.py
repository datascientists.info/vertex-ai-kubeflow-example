from typing import Dict

from kfp.v2.dsl import component
from kfp.v2.dsl import Input, Dataset, Model, OutputPath


@component(
    output_component_file="yaml_tmp/predict_threshold.yaml",
    base_image="python:3.8",
    packages_to_install=[
        "google-cloud-bigquery",
        "google-cloud-storage",
        "pandas",
        "pandas-gbq",
        "numpy",
        "pymc3==3.9.3",
        "arviz==0.11.0",
        "joblib",
        "loguru",
        "scipy",
        "scikit-learn",
    ],
)
def predict_threshold(
    input_data: Input[Dataset],
    input_model: Input[Model],
    super_region: str,
    scooter_type: str,
    parameters: Dict,
    bucket_name: str,
    permanent_path: str,
    out_data_path: OutputPath("Dataset"),
    super_region_ids: Dict,
    vehicle_ids: Dict,
):
    """
    predict threshold for super region x scooter type combination and write result to gcs for loading into bigquery
    :param input_data: scooter data for super region
    :param input_model: reliability model for prediction
    :param super_region: super region name for prediction
    :param scooter_type: scooter type for prediction
    :param parameters: model parameters
    :param bucket_name: name of bucket to store predicted data in
    :param permanent_path: path to store predicted data in
    :param out_data_path: kfp internal path to store data
    :param super_region_ids: dict of relation super_region_name -> id
    :param vehicle_ids: dict of relation vehicle_name -> id
    :return:
    """
    import pandas as pd
    import numpy as np
    import joblib
    import math
    import time
    from google.cloud import storage
    from loguru import logger

    from scipy.interpolate import interp1d
    from sklearn.mixture import GaussianMixture

    def load_reliability(gcs_path):
        model_name = (
            f"/gcs/{gcs_path}"
            + super_region.replace(" ", "_")
            + "_"
            + scooter_type
            + ".joblib"
        )
        with open(model_name, "rb") as buffer:
            l_loaded_data = joblib.load(buffer)
        return l_loaded_data

    df = pd.read_csv(
        input_data.path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv"
    )

    try:
        gm = GaussianMixture(n_components=2, random_state=0).fit(
            df["reliability"].values.reshape(-1, 1)
        )
        reliability_threshold = np.squeeze(
            gm.means_[0] - 3 * np.sqrt(gm.covariances_)[0]
        )

        logger.info("loading model")
        logger.info(
            input_model.uri.replace(
                super_region.replace(" ", "_") + "_" + scooter_type, ""
            )[5:]
        )

        loaded_data = load_reliability(
            input_model.uri.replace(
                super_region.replace(" ", "_") + "_" + scooter_type, ""
            )[5:]
        )

        logger.info("loaded model")
        logger.info(loaded_data)

        y = np.array(range(100, 0, -1))
        x = loaded_data["reliability"]
        interpolate_func = interp1d(x, y, kind="linear", fill_value="extrapolate")

        if reliability_threshold < parameters["training"]["LB_RELIABILITY"]:
            reliability_threshold = parameters["training"]["LB_RELIABILITY"]
        threshold = interpolate_func(reliability_threshold)
        is_default = False
    except Exception as e:
        logger.info(str(e))
        threshold = parameters["training"]["DEFAULT_THRESHOLD"]
        is_default = True

    if threshold < 19:
        threshold = 19
    elif threshold > 30:
        threshold = 30
    else:
        try:
            threshold = math.floor(threshold)
        except Exception as e:
            logger.info("cloud not floor value")
            logger.info(str(e))
            threshold = parameters["training"]["DEFAULT_THRESHOLD"]

    super_region_id = super_region_ids[super_region]
    vehicle_version_id = vehicle_ids[scooter_type]

    timestamp = int(time.time())

    df = pd.DataFrame(
        [
            {
                "super_region_id": super_region_id,
                "vehicle_version_id": vehicle_version_id,
                "reliability_pct": int(parameters["training"]["LB_RELIABILITY"] * 100),
                "threshold_pct": threshold,
                "created_at": timestamp,
                "is_default": is_default,
            }
        ]
    )

    df.to_csv(
        out_data_path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv",
        index=False,
    )

    # upload model to fixed location
    client = storage.Client()

    bucket = client.get_bucket(bucket_name)
    object_name_in_gcs_bucket = bucket.blob(
        permanent_path
        + "threshold_"
        + super_region.replace(" ", "_")
        + "_"
        + scooter_type
        + ".csv"
    )
    object_name_in_gcs_bucket.upload_from_filename(
        out_data_path + super_region.replace(" ", "_") + "_" + scooter_type + ".csv",
    )
