parameters = {
    "training": {
        "NUM_SAMPLES": 2000,
        "FRACTION": 0.5,
        "N_CORES": 4,
        "N_CHAINS": 4,
        "BURNING_FRACTION": 0.2,
        "SEED": 3,
        "LB_RELIABILITY": 0.5,
        "DEFAULT_THRESHOLD": 24,
    },
}
