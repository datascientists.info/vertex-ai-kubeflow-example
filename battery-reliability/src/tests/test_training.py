import unittest
from collections import namedtuple
import joblib
import os

from mock import patch

patch(
    "kfp.v2.dsl.component",
    lambda *x, **y: lambda f: f,
).start()

from ..components.data_science.training import train_base, retrain
from .parameters import parameters


BUCKET = "kedro-test-spin"
if os.getenv("BRANCH_NAME") == "main":
    BUCKET = "spin-vertex-ai"


class Metrics:
    def __init__(self):
        1

    def log_metric(self, param, value):
        None


class TestTrain(unittest.TestCase):
    def test_train_base(self):
        metrics = Metrics()

        InputStruct = namedtuple("InputStruct", "path")
        s = InputStruct(path="./tests/data/train_base_input/")
        OutputStruct = namedtuple("OutputStruct", "path")
        o = OutputStruct(path="./tests/data/train_base_output/")
        t = train_base(
            input_data=s,
            super_region="San Francisco",
            scooter_type="Ninebot",
            bucket_name=BUCKET,
            permanent_path="test-models/",
            parameters=parameters,
            metrics=metrics,
            out_model=o,
        )

        with open(
            "./tests/data/train_base_output/_San_Francisco_Ninebot.joblib", "rb"
        ) as buff:
            loaded_data = joblib.load(buff)

        with open(
            "./tests/data/train_base_output/_San_Francisco_Ninebot_original.joblib",
            "rb",
        ) as buff:
            loaded_data_original = joblib.load(buff)

        self.assertDictEqual(loaded_data["eap"], loaded_data_original["eap"])

    def test_retrain(self):
        InputStruct = namedtuple("InputStruct", "path")
        s = InputStruct(path="./tests/data/retrain_input/")
        OutputStruct = namedtuple("OutputStruct", "path")
        o = OutputStruct(path="./tests/data/retrain_output/")
        metrics = Metrics()

        t = retrain(
            input_data=s,
            super_region="San Francisco",
            scooter_type="Ninebot",
            bucket_name=BUCKET,
            permanent_path="test-models/",
            parameters=parameters,
            metrics=metrics,
            out_model=o,
        )

        with open(
            "./tests/data/retrain_output/_San_Francisco_Ninebot.joblib", "rb"
        ) as buff:
            loaded_data = joblib.load(buff)

        with open(
            "./tests/data/retrain_output/_San_Francisco_Ninebot_original.joblib",
            "rb",
        ) as buff:
            loaded_data_original = joblib.load(buff)

        self.assertDictEqual(loaded_data["eap"], loaded_data_original["eap"])

        # self.assertEqual(True, False)


if __name__ == "__main__":
    unittest.main()
