import unittest
from collections import namedtuple
import joblib
from pathlib import Path
import shutil

from mock import patch

patch(
    "kfp.v2.dsl.component",
    lambda *x, **y: lambda f: f,
).start()

from ..components.data_science.get_reliability import get_reliability


class TestReliability(unittest.TestCase):
    def test_reliability(self):
        # prepare directory structure
        path = Path("/gcs/ts/data/reliability_input")
        path.mkdir(parents=True, exist_ok=True)
        shutil.copy(
            "./tests/data/reliability_input/_San_Francisco_Ninebot.joblib",
            "/gcs/ts/data/reliability_input/",
        )

        InputStruct = namedtuple("InputStruct", "uri")
        m = InputStruct(uri="./tests/data/reliability_input/")
        OutputStruct = namedtuple("OutputStruct", "path")
        o = OutputStruct(path="./tests/data/reliability_output/")

        r = get_reliability(
            in_model=m,
            out_model=o,
            super_region="San Francisco",
            scooter_type="Ninebot",
            battery_obs=None,
            t=24,
        )

        with open(
            "./tests/data/reliability_output/San_Francisco_Ninebot.joblib", "rb"
        ) as buff:
            loaded_data = joblib.load(buff)

        with open(
            "./tests/data/reliability_output/San_Francisco_Ninebot_original.joblib",
            "rb",
        ) as buff:
            loaded_data_original = joblib.load(buff)

        self.assertListEqual(
            loaded_data["reliability"], loaded_data_original["reliability"]
        )


if __name__ == "__main__":
    unittest.main()
