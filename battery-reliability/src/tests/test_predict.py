import os
import unittest
from collections import namedtuple
from pathlib import Path
import shutil
import pandas as pd
from pandas.testing import assert_frame_equal
import os

from mock import patch

patch(
    "kfp.v2.dsl.component",
    lambda *x, **y: lambda f: f,
).start()

from ..components.data_science.predict import predict


BUCKET = "kedro-test-spin"
if os.getenv("BRANCH_NAME") == "main":
    BUCKET = "spin-vertex-ai"


class TestPredict(unittest.TestCase):
    def test_predict(self):
        # prepare directory structure
        path = Path("/gcs/ts/data/predict_input/")
        path.mkdir(parents=True, exist_ok=True)
        shutil.copy(
            "./tests/data/predict_input/San_Francisco_Ninebot.joblib",
            "/gcs/ts/data/predict_input/",
        )

        InputStruct = namedtuple("InputStruct", "path")
        s = InputStruct(path="./tests/data/predict_input/")
        InputStruct = namedtuple("InputStruct", "uri")
        m = InputStruct(uri="./tests/data/predict_input/")
        OutputStruct = namedtuple("OutputStruct", "path")
        o = OutputStruct(path="./tests/data/predict_output/")

        p = predict(
            model=m,
            data=s,
            super_region="San Francisco",
            scooter_type="Ninebot",
            output_data_path="./tests/data/predict_output/",
            bucket_name=BUCKET,
            permanent_path="test-bq-load/",
        )

        result = pd.read_csv("./tests/data/predict_output/San_Francisco_Ninebot.csv")
        result.drop("meta_insert_ts", inplace=True, axis=1)
        original = pd.read_csv(
            "./tests/data/predict_output/San_Francisco_Ninebot_original.csv"
        )
        original.drop("meta_insert_ts", inplace=True, axis=1)

        assert_frame_equal(result, original)


if __name__ == "__main__":
    unittest.main()
