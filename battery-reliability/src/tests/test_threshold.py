import unittest
from collections import namedtuple
import pandas as pd
from pandas.testing import assert_frame_equal
import os


from mock import patch

patch(
    "kfp.v2.dsl.component",
    lambda *x, **y: lambda f: f,
).start()

from ..components.data_science.predict_threshold import predict_threshold
from .parameters import parameters


BUCKET = "kedro-test-spin"
if os.getenv("BRANCH_NAME") == "main":
    BUCKET = "spin-vertex-ai"


class TestThreshold(unittest.TestCase):
    def test_threshold(self):
        InputStruct = namedtuple("InputStruct", "path")
        s = InputStruct(path="./tests/data/predict_threshold_input/")
        InputStruct = namedtuple("InputStruct", "uri")
        m = InputStruct(uri="./tests/data/predict_threshold_input/")
        super_region_ids = {"San Francisco": 1}
        vehicle_ids = {"NinebotMaxV21": 1}

        p = predict_threshold(
            input_data=s,
            input_model=m,
            super_region="San Francisco",
            scooter_type="NinebotMaxV21",
            parameters=parameters,
            out_data_path="./tests/data/predict_threshold_output/",
            super_region_ids=super_region_ids,
            vehicle_ids=vehicle_ids,
            bucket_name=BUCKET,
            permanent_path="test-bq-load/",
        )

        result = pd.read_csv(
            "./tests/data/predict_threshold_output/San_Francisco_NinebotMaxV21.csv"
        )

        result.drop("created_at", axis=1, inplace=True)

        original = pd.read_csv(
            "./tests/data/predict_threshold_output/San_Francisco_NinebotMaxV21_original.csv"
        )

        original.drop("created_at", axis=1, inplace=True)

        assert_frame_equal(result, original)


if __name__ == "__main__":
    unittest.main()
