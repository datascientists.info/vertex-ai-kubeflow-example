import unittest
from collections import namedtuple
import pandas as pd
from pandas.testing import assert_frame_equal

from mock import patch

patch(
    "kfp.v2.dsl.component",
    lambda *x, **y: lambda f: f,
).start()

from ..components.data_engineering.preprocess import preprocess
from .parameters import parameters


class TestPreprocess(unittest.TestCase):
    def test_preprocess(self):
        InputStruct = namedtuple("InputStruct", "path")
        s = InputStruct(path="./tests/data/preprocess_input/")

        pre = preprocess(
            parameters=parameters,
            super_region="San Francisco",
            scooter_type="Ninebot",
            input_path=s,
            output_path="./tests/data/preprocess_output/",
        )

        result = pd.read_csv("./tests/data/preprocess_output/San_Francisco_Ninebot.csv")
        original = pd.read_csv(
            "./tests/data/preprocess_output/San_Francisco_Ninebot_original.csv"
        )

        assert_frame_equal(result, original)


if __name__ == "__main__":
    unittest.main()
