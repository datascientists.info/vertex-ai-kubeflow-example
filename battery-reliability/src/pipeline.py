from kfp import dsl
from typing import Dict

from components.data_engineering.load_data import (
    load_data,
    get_super_regions,
    get_vehicles,
    get_super_regionid,
    get_vehicleid,
)
from components.data_engineering.preprocess import preprocess
from components.data_science.training import train_base, retrain, train_base2
from components.data_science.get_reliability import get_reliability
from components.data_engineering.get_batch_data import get_batch_data
from components.data_science.predict import predict
from components.data_engineering.load_result_to_bq import load_from_gcs_to_bq
from components.data_science.predict_threshold import predict_threshold


@dsl.pipeline(
    name="battery-reliability",
    pipeline_root="gs://BUCKET_NAME",
)
def pipeline(
    project: str = "PROJECT_ID",
    min_data_required: int = 100,
    bucket: str = "BUCKET_NAME",
    permanent_path: str = "models/",
    permanent_path_bq_load: str = "model-bq-load/",
    parameters: Dict = {
        "training": {
            "FRACTION": 0.5,
            "N_CORES": 4,
            "N_CHAINS": 4,
            "BURNING_FRACTION": 0.2,
            "SEED": 3,
            "LB_RELIABILITY": 0.5,
            "DEFAULT_THRESHOLD": 24,
        }
    },
    battery_threshold_table: str = "test_ml_outputs.vertex_ai_battery_threshold",
    battery_reliability_table: str = "test_ml_outputs.vertex_ai_battery_reliability",
):
    print(project)
    super_region_ids = get_super_regionid(project=project)
    vehicle_ids = get_vehicleid(project=str(project))
    regions = get_super_regions(project=project)
    with dsl.ParallelFor(regions.outputs["regions"]) as super_region:
        vehicles = get_vehicles(project=project, super_region=super_region)
        with dsl.ParallelFor(vehicles.outputs["vehicles"]) as scooter_type:
            load = load_data(
                super_region=super_region,
                scooter_type=scooter_type,
                min_data_required=min_data_required,
                bucket_name=bucket,
                permanent_path=permanent_path,
                project=project,
            )

            with dsl.Condition(load.outputs["enough_rows"] == "true"):
                pre = preprocess(
                    parameters=parameters,
                    super_region=super_region,
                    scooter_type=scooter_type,
                    input_path=load.outputs["out_data_path"],
                )
                with dsl.Condition(pre.outputs["preprocess_empty"] == "false"):
                    with dsl.Condition(load.outputs["model_exists"] == "false"):
                        train = train_base(
                            input_data=pre.outputs["output_path"],
                            parameters=parameters,
                            super_region=super_region,
                            scooter_type=scooter_type,
                            bucket_name=bucket,
                            permanent_path=permanent_path,
                        )
                        train.set_memory_limit("32G")
                        train.set_cpu_limit("8")

                        reliability = get_reliability(
                            in_model=train.outputs["out_model"],
                            super_region=super_region,
                            scooter_type=scooter_type,
                        )

                        with dsl.Condition(
                            reliability.outputs["reliability_elements"] == "true"
                        ):
                            batch_data = get_batch_data(
                                super_region=super_region,
                                scooter_type=scooter_type,
                                project=project,
                            )
                            pred = predict(
                                model=reliability.outputs["out_model"],
                                data=batch_data.outputs["batch_data_path"],
                                super_region=super_region,
                                scooter_type=scooter_type,
                                bucket_name=bucket,
                                permanent_path=permanent_path_bq_load,
                            )
                            pred_th = predict_threshold(
                                input_data=pred.outputs["output_data_path"],
                                input_model=reliability.outputs["out_model"],
                                super_region=super_region,
                                scooter_type=scooter_type,
                                parameters=parameters,
                                super_region_ids=super_region_ids.outputs[
                                    "super_region_ids"
                                ],
                                vehicle_ids=vehicle_ids.outputs["vehicle_ids"],
                                bucket_name=bucket,
                                permanent_path=permanent_path_bq_load,
                            )

                    with dsl.Condition(load.outputs["model_exists"] == "true"):
                        r_train = retrain(
                            input_data=pre.outputs["output_path"],
                            parameters=parameters,
                            super_region=super_region,
                            scooter_type=scooter_type,
                            bucket_name=bucket,
                            permanent_path=permanent_path,
                        )
                        r_train.set_memory_limit("32G")
                        r_train.set_cpu_limit("8")
                        with dsl.Condition(
                            r_train.outputs["retrain_possible"] == "false"
                        ):
                            train2 = train_base2(
                                input_data=pre.outputs["output_path"],
                                parameters=parameters,
                                super_region=super_region,
                                scooter_type=scooter_type,
                                bucket_name=bucket,
                                permanent_path=permanent_path,
                            )
                            train2.set_memory_limit("32G")
                            train2.set_cpu_limit("8")

                            reliability = get_reliability(
                                in_model=train2.outputs["out_model"],
                                super_region=super_region,
                                scooter_type=scooter_type,
                            )

                            with dsl.Condition(
                                reliability.outputs["reliability_elements"] == "true"
                            ):
                                batch_data = get_batch_data(
                                    super_region=super_region,
                                    scooter_type=scooter_type,
                                    project=project,
                                )
                                pred = predict(
                                    model=reliability.outputs["out_model"],
                                    data=batch_data.outputs["batch_data_path"],
                                    super_region=super_region,
                                    scooter_type=scooter_type,
                                    bucket_name=bucket,
                                    permanent_path=permanent_path_bq_load,
                                )
                                pred_th = predict_threshold(
                                    input_data=pred.outputs["output_data_path"],
                                    input_model=reliability.outputs["out_model"],
                                    super_region=super_region,
                                    scooter_type=scooter_type,
                                    parameters=parameters,
                                    super_region_ids=super_region_ids.outputs[
                                        "super_region_ids"
                                    ],
                                    vehicle_ids=vehicle_ids.outputs["vehicle_ids"],
                                    bucket_name=bucket,
                                    permanent_path=permanent_path_bq_load,
                                )
                        with dsl.Condition(
                            r_train.outputs["retrain_possible"] == "true"
                        ):
                            reliability = get_reliability(
                                in_model=r_train.outputs["out_model"],
                                super_region=super_region,
                                scooter_type=scooter_type,
                            )

                            with dsl.Condition(
                                reliability.outputs["reliability_elements"] == "true"
                            ):
                                batch_data = get_batch_data(
                                    super_region=super_region,
                                    scooter_type=scooter_type,
                                    project=project,
                                )
                                pred = predict(
                                    model=reliability.outputs["out_model"],
                                    data=batch_data.outputs["batch_data_path"],
                                    super_region=super_region,
                                    scooter_type=scooter_type,
                                    bucket_name=bucket,
                                    permanent_path=permanent_path_bq_load,
                                )
                                pred_th = predict_threshold(
                                    input_data=pred.outputs["output_data_path"],
                                    input_model=reliability.outputs["out_model"],
                                    super_region=super_region,
                                    scooter_type=scooter_type,
                                    parameters=parameters,
                                    super_region_ids=super_region_ids.outputs[
                                        "super_region_ids"
                                    ],
                                    vehicle_ids=vehicle_ids.outputs["vehicle_ids"],
                                    bucket_name=bucket,
                                    permanent_path=permanent_path_bq_load,
                                )

    bq_load = load_from_gcs_to_bq(
        table_name=battery_threshold_table,
        project=project,
        permanent_path=permanent_path_bq_load,
        bucket_name=bucket,
        prefix="threshold_",
    ).after(pred_th)

    bq_load = load_from_gcs_to_bq(
        table_name=battery_reliability_table,
        project=project,
        permanent_path=permanent_path_bq_load,
        bucket_name=bucket,
        prefix="predict_",
    ).after(pred)
