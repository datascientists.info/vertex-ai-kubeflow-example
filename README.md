# Vertex AI machine learning ops

Training and deploying models using Vertex AI (https://cloud.google.com/vertex-ai/) and Kubeflow 
(https://www.kubeflow.org/).

## Machine Learning Ops
| Component | Description | Vertex AI |
| --- | --- | --- |
| Artifact Store | Storing all necessary artifacts, e.g. models and data | Vertex ML Metadata |
| ML Metadata | training parameters, evaluation metrics, prediction examples, dataset versions, testing pipeline outputs, references to model weights files, and other model-related things. | Vertex ML Metadata |
| Experimentation | Preliminary data analysis and experimentation with models | Jupyter Notebook |
| ML Pipeline | A machine learning pipeline is a way to codify and automate the workflow it takes to produce a machine learning model. Machine learning pipelines consist of multiple sequential steps that do everything from data extraction and preprocessing to model training and deployment. | KubeFlow Serverless with UI |
| Model Serving | making models available for usage in data products | Serverless API |
| Model Versioning | keep track of changing models | Vertex ML Metadata |
| Source Code Versioning | | Git / self managed |


## Vertex AI
Vertex AI is an integrated platform for Machine Learning Ops, that includes everything needed for
developing and deploying machine learning model.
As a serverless service resource handling is effortless and on-demand. 

## Kubeflow
Kubeflow is a framework to easily transform python code (and docker containers) into pipelines 
executable on a Kubernetes Cluster.
Kubeflow works with python decorators, if native Python code is used. To use those with Vertex AI, 
it has to be Kubeflow V2, mainly for creating the pipeline definition JSON.

```
from kfp import dsl
from kfp.v2 import compiler

@dsl.pipeline(
    name="vertex-pipeline",
    pipeline_root='gs://bucket-name',
)
def pipeline():
    func = call_node_function()
 
compiler.Compiler().compile(
    pipeline_func=pipeline, package_path="test_pipeline.json".replace(" ", "_")
)
```

## Pipeline Development
To efficiently train a model using Kubeflow Pipelines the code needs to be split into small steps in
python functions. Each of these functions should be self containing with input and ouptput parameters, 
that are in turn used by other steps.

To make communications between different nodes easier, use Kubeflow typing hints (https://www.kubeflow.org/docs/components/pipelines/sdk-v2/v2-component-io/)

```
def function_name(
    in_data: Input[Dataset],
    out_model: Output[Model],
):
```

### KFP node from python function
To transform a Python function into a Kubeflow Node you have to add a ```@component``` decorated to it.

```
@component(
    packages_to_install=["pandas", "numpy"],
    base_image="python:3.9",
    output_component_file="deploy_component.yaml",
)
```

Parameters:
* packages_to_install: packages needed to run this one function
* base_image: name of image to use for node
* output_component_file: name of temporary yaml file that get created during compilation into pipeline
  json.

Please note that all imports needed inside a function need to be defined inside the function and
not at the top of the file.

## Pipeline Deployment
Pipelines are deployed to VertexAI using the VertexAI Python SDK

```
import google.cloud.aiplatform as aip
```

### Create a pipeline job

```
job = aip.PipelineJob(
    display_name="pipeline-name",
    template_path="compiled_pipeline.json",
    pipeline_root='gs://bucket-name',
    project="gcp-project",
    location="us-east1",
    credentials=google.oauth2.credentials.Credentials(
        creds.token
    ),
    enable_caching=True,
)
```

#### Parameters
* display_name: base of name for VertexAI pipeline. If it is not changed VertexAI recognises each new
  deployment as a new version or other run for the same pipeline.
* template_path: path to json file generated using the kfp.v2.compiler
* enable_caching: if set to true, VertexAI reuses output data from previous runs, if nothing has changed
  code wise. Speeds up development, should be set to __False__ in production environments.

### Run the pipeline

```
job.run()
```

All files (temporary and defined in the pipeline) are stored inside the ```pipeline_root``` bucket in this way:
```
gcs://pipeline_root/service_account_number/display_name-timestamp/
```

Inside the process files from that bucket are available using the following (non-gcs) path:
```
/gcs/pipeline_root/filename.txt
```