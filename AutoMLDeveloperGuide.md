# AutoML Developer Guide

Google's AutoML helps with building machine learning models, by chosing the best model by itself. This can be used
in the following way.

## AutoML option
AutoML can be used on the following types of input data:
* Structured data (AutoML Tabular)
* Sight (AutoML Image and AutoML Video)
* Language (AutoML Text and AutoML Translation)

Deeper insight into these can be found here: https://cloud.google.com/automl

In this guide we will concentrate on structured data, like Bigquery as a source.

## Datasource
Selecting a fitting data source is the single most important task for AutoML usage, since this is the only way your 
experience and business knowledge can influence the resulting model.

### Manual work
There is some manual work involved in creating the datasource for AutoML. This includes:
* define target value: This value has to be either numerical or categorical. If you chose categorical you need to have 
  between 2 and 500 values.
* select attributes you want to include in your model
* maybe apply binning on attributes (https://de.wikipedia.org/wiki/Binning)
* think about missing value handling, if you want to do that manually or have AutoML take are of it (https://towardsdatascience.com/7-ways-to-handle-missing-values-in-machine-learning-1a6326adf79e)

### Make data available in VertexAI
Once the above steps are done, you need to make the data available in VertexAI. The first step here is to create
a view in Bigquery. This view can then be defined as a source in VertexAI. Either by using the UI (https://console.cloud.google.com/vertex-ai/datasets?project=data-234917)
or programmatically in your pipeline script.

```
from google_cloud_pipeline_components import aiplatform as gcc_aip

dataset_create_op = gcc_aip.TabularDatasetCreateOp(
    project=project,
    display_name=display_name,
    bq_source=training_data_table,
)
```

### Train AutoML Model
To train a model you need to programm in Python, e.g. like this:
```
from google_cloud_pipeline_components import aiplatform as gcc_aip

training_op = gcc_aip.AutoMLTabularTrainingJobRunOp(
    project=project,
    display_name=display_name,
    optimization_prediction_type="classification",
    budget_milli_node_hours=budget_milli_node_hours,
    column_transformations=[
        {"categorical": {"column_name": "super_region_name"}},
        {"categorical": {"column_name": "possession_status"}},
        {"categorical": {"column_name": "lock_state"}},
        {"categorical": {"column_name": "primary_operations_status"}},
        {"numeric": {"column_name": "batt_percentage"}},
        {"categorical": {"column_name": "is_active"}},
        {"categorical": {"column_name": "is_offline"}},
        {"categorical": {"column_name": "admin_zone"}},
        {"categorical": {"column_name": "action"}},
        {"categorical": {"column_name": "start_admin_zone"}},
        {"categorical": {"column_name": "end_admin_zone"}},
        {"categorical": {"column_name": "trip_exceeds_max_time"}},
        {"categorical": {"column_name": "trip_exceeds_max_bc_distance"}},
        {"numeric": {"column_name": "user_trip_num_all_time"}},
        {"numeric": {"column_name": "user_trip_num_that_day"}},
        {"numeric": {"column_name": "vehicle_trip_num_that_day"}},
        {"numeric": {"column_name": "start_batt_percentage"}},
        {"numeric": {"column_name": "end_batt_percentage"}},
        {"categorical": {"column_name": "last_activity_by"}},
        {"numeric": {"column_name": "days_in_state"}},
        {"numeric": {"column_name": "label"}},
    ],
    dataset=dataset_create_op.outputs["dataset"],
    target_column="label",
)
```

Important here are the following paramter:
* optimization_prediction_type: can be either "classification" or "forecasting"
* budget_milli_node_hours: The train budget of creating this Model, expressed in milli node hours i.e. 1,000 value in 
  this field means 1 node hour. The training cost of the model will not exceed this budget.
* column_transformations: List[Dict[str, Dict[str, str]]] Optional. Transformations to apply to the input columns (i.e. 
  columns other than the targetColumn). Each transformation may produce multiple result values from the column's value, 
  and all are used for training.

## Evaluate models
AutoML automatically stores evaluation metrics for all evaluated models in the result of the training. These metrics
can be accessed like this

```
from google.cloud import automl_v1beta1 as automl

client = automl.TablesClient(project=project_id, region=compute_region)

# List all the model evaluations in the model by applying filter.
response = client.list_model_evaluations(
    model_display_name=model_display_name, filter=filter
)

print("List of model evaluations:")
for evaluation in response:
    print("Model evaluation name: {}".format(evaluation.name))
    print("Model evaluation id: {}".format(evaluation.name.split("/")[-1]))
    print(
        "Model evaluation example count: {}".format(
            evaluation.evaluated_example_count
        )
    )
    print("Model evaluation time: {}".format(evaluation.create_time))
    print("\n")
```

For more information check the documentation here:  
https://cloud.google.com/automl-tables/docs/evaluate

## Deployment / Usage of model
There are two ways of getting a prediction from an AutoML model, online and offline. 

Online predictions are used for single row prediction or several rows with a result in real time.

https://cloud.google.com/automl-tables/docs/predict#automl-tables-example-python

Offline predictions can be used if the data is stored inside a file and can be uploaded to Google Cloud storage.

https://cloud.google.com/automl-tables/docs/predict-batch